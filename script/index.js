import LoginModal from "./classes/classloginModal.js"
import Select from "./classes/classSelect.js"
import Form from "./classes/classForm.js"
import Input from "./classes/classInput.js"
import Visit from "./classes/classVisit.js"
import filterCards from "./filterCards.js"



const searchingBlock = document.getElementById("searching-block");
const inputSearchForm = new Input("search-input filter","Search by title","search-block","text","search-input","search-input");
const selectTimeBody={
    className:"select-block-search filter",
      divClassName:"search-block",
       id:"select-block-time",
        value:["all","open","done"]
}
const selectBlockTime = new Select(selectTimeBody)
const selectPriorityBody={
    className:"select-block-search filter",
      divClassName:"search-block",
       id:"select-block-priority",
        value:["all","Обычная","Приоритетная","Неотложная"]
}
const selectBlockPriority = new Select(selectPriorityBody);

searchingBlock.insertAdjacentHTML("afterbegin",`${inputSearchForm.render()}${selectBlockTime.render()}
${selectBlockPriority.render()}`)
const btnPrimaryProps = {
    classList: ["login-form", "login-modal"],
    id: "login-form",
    name:"login-form"
};

const filterList = document.querySelectorAll(".filter");
    filterList.forEach(i=>{
        i.addEventListener("change",filterCards);
    });

const loginFormModal = new LoginModal(btnPrimaryProps);
const loginContainer = document.getElementById("login-container");
loginContainer.append(loginFormModal.render())

const loginSubmitForm = document.getElementById("login-submit");
loginSubmitForm.addEventListener("submit",loginFormModal.loginPost.bind(loginFormModal));

const loginModal = document.getElementById("nav-log-in");
loginModal.addEventListener("click",function(){
    const loginForm = document.getElementById("login-form");
    loginForm.classList.toggle("active")
})

const token = localStorage.getItem('token');
const req = axios.create({
    baseURL: 'http://cards.danit.com.ua',
    headers: {
        Authorization: `Bearer ${token}`
    }
});

export default async function getCards(){
    const {data} = await req.get("/cards");
    data.forEach(itemCard=>{
        Visit.renderCard(itemCard)
    })
}
const createCard = document.getElementById("create-card");
if(token){
    document.querySelector(".card-list").textContent = "";
    document.querySelector("#nav-log-in").classList.remove("active")
    createCard.classList.add("active");
    getCards()
}else{
    document.querySelector("#nav-log-in").classList.add("active")
}



createCard.addEventListener("click",function(){
    const container = document.querySelector("#create-card-form");
    new Visit().render();
    container.classList.add("active");
})
