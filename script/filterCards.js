import Visit from "./classes/classVisit.js"
const token = localStorage.getItem('token');
const req = axios.create({
    baseURL: 'http://cards.danit.com.ua',
    headers: {
        Authorization: `Bearer ${token}`
    }
});

export default async function filterCards(){
    const {data} = await req.get("/cards");
  const newArr =   data.filter(el=>{
        if(this.id == "search-input"){
            if (!this.value)
            {return el}
            for (let i in el ){
                if (el[i].includes(this.value)){
                    return el;
                }
                
            }
        }else if(this.id === "select-block-time"){

            if( el.status == this.value){
                return  el;
            }else if(this.value === "all"){
                return el
            }
            
        }else{
            if(el.urgencySelect == this.value){
                 return  el;
            }else if(this.value === "all"){
                return el
            }
            
        }
    })
    const divCardList = document.querySelector(".card-list");
    divCardList.innerHTML = "";
    newArr.forEach(card=>{
        Visit.renderCard(card)
    })
}