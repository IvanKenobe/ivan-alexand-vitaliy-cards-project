export default class Modal {
    constructor({classList,id,name}){
        this.classList = classList;
        this.id=id;
        this.name= name;
        this.elem=null;
    }
    render(){
        this.elem=document.createElement("div");
        const {elem} = this;
        elem.className = (this.classList.join(" "));
        elem.id=this.id;
    }
}