 import Drag from "./classDragAndDrop.js"
 import Select from "./classSelect.js"
 import Input from "./classInput.js"
 import Form from "./classForm.js"
 // import currentInfo from "./curerntInfo.js"

 export default class Visit {
    constructor (){
        this.visitForm = document.querySelector("#create-card-form");
        this.closeForm = null;
        this.submitForm = null;
        this.visitForma = null;
    }
    
    render(id= null){
        const doctorCategories = {
            className: "select-block test",
            divClassName: "form-item",
            id: "doctorCategory",
            value:["Стоматолог","Кардиолог","Терапевт"],
            name: "doctorSelect"};

        const urgencyCategories = {
            className: "select-block test",
            divClassName: "form-item",
            id: "urgency",
            value:["Обычная","Приоритетная","Неотложная"],
            name: "urgencySelect"};

        this.visitForm.innerHTML = `<h1 class="login-title">Create a card</h1>`;
        this.visitForma = new Form("create-visit","createVisitForm");
        const createVisitforma = this.visitForma.render();
        const fio = new Input("form-control test","ФИО","form-item","text","visitFio","fio").render();
        const doctorSelect = new Select(doctorCategories).render();
        const visitTarget =  new Input("form-control test","Цель визита","form-item","text","target","visitTarget").render();
        const visitDescription = new Input("form-control test","Краткое описание визита","form-item","text","visitDescription","description").render();
        const urgency = new Select(urgencyCategories).render();
        const additionalCategoriesContainer = document.createElement("div");
        additionalCategoriesContainer.classList.add("aditional-container");

        this.visitForm.append(createVisitforma);
        createVisitforma.insertAdjacentHTML("beforeend",fio);
        createVisitforma.insertAdjacentHTML("beforeend",doctorSelect);
        createVisitforma.insertAdjacentHTML("beforeend",visitTarget);
        createVisitforma.insertAdjacentHTML("beforeend",visitDescription);
        createVisitforma.insertAdjacentHTML("beforeend",urgency);
        createVisitforma.append(additionalCategoriesContainer);
        createVisitforma.insertAdjacentHTML("beforeend",`<div class="card-buttons">
        <button class="btn-create btn-card" id="createVisitBtn" type="submit">Создать</button>
        <button class="btn-cancel btn-card" id="closeCreateVisitForm">Отмена</button>
        </div>`);
        const container = this.visitForm.querySelector(".aditional-container");
        container.innerHTML = new Input("form-control test","Дата последнего визита","form-item","text","lastVisitDate","target").render();
        
        const doctor = this.visitForm.querySelector("#doctorCategory");
        doctor.addEventListener("change",this.doctorChange);

        this.closeForm = document.querySelector("#closeCreateVisitForm");
        this.closeForm.addEventListener("click",this.closeAndClearForm.bind(this));

        this.submit = createVisitforma;
        if (!id){
            this.submit.addEventListener("submit", this.addCard.bind(this));
        } else {
            this.submit.addEventListener("submit", (e)=> this.changeCard(e,id));
        }  
    }

    async addCard(e){
        e.preventDefault();
        this.closeAndClearForm();
        const body = this.visitForma.serializeJSON(e.target);
        body.status = "open";

        const token = localStorage.getItem('token');
        const req = axios.create({
        baseURL: 'http://cards.danit.com.ua',
        headers: {
            Authorization: `Bearer ${token}`
        }
        });
        const data = await req.post("/cards",body).then();
        console.log(data)
        if (data.status < 300){
            Visit.renderCard(data.data);
        }
        
    }
    
    static renderCard(data){
        const card = document.createElement("div");
        card.classList.add("card-block");
        
		card.setAttribute('draggable', 'true');
        const invisibleProp = document.createElement("div");
        invisibleProp.classList.add("invisible-prop-container");
        delete data.statusText;
        for (let key in data){
            if (key === "id") {
                card.setAttribute("data-id",`${data[key]}`)
            } else if (key === "fio" || key === "doctorSelect"){
                card.insertAdjacentHTML(`afterbegin`,`<span class="card-title" data-info="${key}">${data[key]}</span>`);
            } else {
                invisibleProp.insertAdjacentHTML(`beforeend`,`<span class="card-title" data-info="${key}">${data[key]}</span>`);
            }
        }
        card.append(invisibleProp);
        card.insertAdjacentHTML(`beforeend`,`<button class="btn-card show-more">Подробнее</button>`);
        card.insertAdjacentHTML(`beforeend`,`<button class="btn-card delete-card">УДАЛИТЬ!</button>`);
        card.insertAdjacentHTML(`beforeend`,`<button class="btn-card edit-card">Изменить!</button>`);
        document.querySelector(".card-list").append(card);
        const showButton = card.querySelector(".show-more");
        const deleteButton = card.querySelector(".delete-card");
        const editButton = card.querySelector(".edit-card");

        new Drag(card);

        showButton.addEventListener("click",(e)=>{
           const aditionalPropsContainer = e.target.closest(".card-block").querySelector(".invisible-prop-container");
           aditionalPropsContainer.classList.toggle("active");
           if(e.target.textContent === "Подробнее"){
               e.target.textContent = "Скрыть доп инфу"
           } else {
            e.target.textContent = "Подробнее";
           }
        })

        deleteButton.addEventListener("click",async (e)=>{
            const card = e.target.closest(".card-block");
            const {id} = card.dataset;
            const token = localStorage.getItem('token');
            const request = await axios.create({
                baseURL: 'http://cards.danit.com.ua',
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });
            request.delete(`/cards/${id}`)
            .then(response => {
                if (response.status < 300){
                    card.remove();
                    const firstCard = document.querySelector(".card-block");
                    if (!firstCard){
                        document.querySelector(".card-list").textContent = "No items have been added";
                    }
                }
            });
           
            
        });
        editButton.addEventListener("click", function (e) {
            const cardBlock = e.target.closest(".card-block")
            const {id} = cardBlock.dataset;
            const card = Array.from(cardBlock.querySelectorAll(".card-title"))
            const elements = card.map(elem => {
                return elem.textContent
            })
            let value1 = elements[0]
            elements[0] = elements[1]
            elements[1] = value1
            const container = document.querySelector("#create-card-form");
            
            new Visit().render(id);
            if (elements[1] === "Кардиолог"){
                new VisitCardiologist().render();
            }
            container.classList.add("active")
            const createVisit = document.querySelectorAll(".test")
            let index = 0;
            createVisit.forEach(elem => {
                elem.value = elements[index]
                index++
            })
            const changeBtn = document.querySelector("#createVisitBtn")
            changeBtn.textContent = "Изменить"
        })
    }

    async changeCard(e,id){
        e.preventDefault();
        this.closeAndClearForm();
        const body = this.visitForma.serializeJSON(e.target);
        body.status = "open";
        const token = localStorage.getItem('token');
        const req = axios.create({
        baseURL: 'http://cards.danit.com.ua/cards',
        headers: {
            Authorization: `Bearer ${token}`
        }
        });
        const response = await req.put(`/${id}`,body).then();
        console.log(response)
        if (response.status < 300){
            document.querySelector(`[data-id="${id}"]`).remove();
            Visit.renderCard(response.data);
        } else {
            alert("Ups Error sorry(");
        }
        
    }

    doctorChange(e){
        if (e.target.value === "Кардиолог"){
            new VisitCardiologist().render();
        } else if (e.target.value === "Стоматолог"){
            new VisitDentist().render();
        } else if (e.target.value === "Терапевт"){
            new VisitTherapist().render();
        }
    }

    closeAndClearForm(){
        this.visitForm.innerHTML = "";
        this.visitForm.classList.remove("active");
    }

}

class VisitDentist extends Visit {
    constructor(visitForm){
        super(visitForm)
    }
    render(){
        const container = this.visitForm.querySelector(".aditional-container");
        container.innerHTML = new Input("form-control test","Дата последнего визита","form-item","text","target","lastVisitDate").render();
    }
}

class VisitCardiologist extends Visit {
    constructor(visitForm){
        super(visitForm)
    }
    render(){
        const container = this.visitForm.querySelector(".aditional-container");
        container.insertAdjacentHTML("beforeend",new Input("form-control test","Возраст","form-item","text","target","age").render());
        container.insertAdjacentHTML("beforeend",new Input("form-control test","Обычное давление","form-item","text","target","pressure").render());
        container.insertAdjacentHTML("beforeend",new Input("form-control test","Индекс массы тела","form-item","text","target","imt").render());
        container.insertAdjacentHTML("beforeend",new Input("form-control test","Перенесенные заболевания сердечно-сосудистой системы","form-item","text","target","heartPain").render());
    }
}

class VisitTherapist extends Visit {
    constructor(visitForm){
        super(visitForm)
    }
    render(){
        const container = this.visitForm.querySelector(".aditional-container");
        container.innerHTML = new Input("form-control test","Возраст","form-item","text","target","age").render();
    }
}