export default class TextArea {
    constructor(className, labelContent, divClassName, type, id, value) {
        this.className = className;
        this.labelContent = labelContent;
        this.type = type;
        this.id = id;
        this.divClassName = divClassName
    }
    render() {
        return `<divclass="${this.divClassName}">
                    <label>${this.labelContent}</label>
                    <textarea class="${this.className}" id="${this.id}" type="${this.type}"></textarea>
                </div>`
    }
}
