
export default class Select{
    constructor({className,  divClassName, id, value, name}) {
        this.className = className;
        this.id = id;
        this.divClassName = divClassName;
        this.name = name;
        this.value = [...value];
    }

    render(){

        return `<div class="${this.divClassName}">
                    <select name="${this.name}" class="${this.className}" id="${this.id}">
                    ${this.value.map(item => {
            return `<option >${item}</option>`
        })}
                    </select>
                </div>`

    }
}



