export default class Drag {
    static dragged = null;
	constructor (element) {
        this.element = element;
		
		element.addEventListener('dragstart', this.dragstart.bind(this));
		element.addEventListener('dragend', this.dragend.bind(this));
		element.addEventListener('dragenter', this.dragenter.bind(this));
		element.addEventListener('dragover', this.dragover.bind(this));
		element.addEventListener('dragleave', this.dragleave.bind(this));
		element.addEventListener('drop', this.drop.bind(this))
	}

	dragstart (event) {
		Drag.dragged = this.element;
		this.element.classList.add('dragged');
		event.stopPropagation();
	}

	dragend (event) {
		event.stopPropagation();
		Drag.dragged = null;
		this.element.classList.remove('dragged');
		document
			.querySelectorAll('.card-block')
			.forEach(x => x.classList.remove('under'));
	}

	dragenter (event) {
		event.stopPropagation();
		if (Drag.dragged || !(this.element === Drag.dragged)) {
			this.element.classList.add('under');
		}
	}

	dragover (event) {
		event.preventDefault();
		event.stopPropagation();
	}

	dragleave (event) {
		event.stopPropagation();
		if (Drag.dragged || !(this.element === Drag.dragged)) {
			this.element.classList.remove('under');
		}
	}

	drop (event) {
		event.stopPropagation();
		if (!Drag.dragged || this.element === Drag.dragged) {
			return;
		}
		if (this.element.parentElement === Drag.dragged.parentElement) {
			const note = Array.from(this.element.parentElement.querySelectorAll('.card-block'));
			const indexA = note.indexOf(this.element);
			const indexB = note.indexOf(Drag.dragged);
			if (indexA < indexB) {
				this.element.parentElement.insertBefore(Drag.dragged, this.element);
			} else {
				this.element.parentElement.insertBefore(Drag.dragged, this.element.nextElementSibling);
			}
		} else {
			this.element.parentElement.insertBefore(Drag.dragged, this.element);
		}
	}
}