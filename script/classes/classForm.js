

export default class Form {
    constructor(classList,id){
        this.classList= classList;
        this.id= id;
        this.elem= null;
    }
    render(){
        this.elem = document.createElement("form");
        const {elem }= this;
        elem.id= this.id;
        elem.className=this.classList;
        return elem;
    }

    serialize(form){
        const body = [];
        form.querySelectorAll('input,  select, textarea').forEach(element => {
            const name = element.getAttribute('name');
            if(name) {
                const target = `${name}=${element.value}`;
               body.push(target);
            } 
        })
        return body.join("&")
    }
    
    serializeJSON(form){
        const body = {}
        form.querySelectorAll('input, select, textarea').forEach(element => {
            const name = element.getAttribute('name');
            if(name) {
                body[name] = element.value;
            }
        });
        return body;
    }
}

