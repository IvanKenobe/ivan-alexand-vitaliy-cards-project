export default class Input {
    constructor(className, placeholder, divClassName, type, id, name) {
        this.className = className;
        this.placeholder = placeholder;
        this.type = type;
        this.id = id;
        this.divClassName = divClassName;
        this.name= name
    }

    render() {
        return `<div class="${this.divClassName}">
                    <input class="${this.className}" placeholder='${this.placeholder}' id="${this.id}" type="${this.type}" name="${this.name}" required>
                </div>`
    }
}

