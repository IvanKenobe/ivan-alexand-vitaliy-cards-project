import Modal from "./classModule.js"
import Form from "./classForm.js"
import getCards from "../index.js"
import Input from "./classInput.js"
export default class LoginModal extends Modal{
    constructor({classList,id,name}){
        super(classList,id,name)
        this.classList=classList;
        this.id=id;
        this.name=name;
        this.form = new Form("login-submit","login-submit",);
        this.formHeader = ` <h1 class="login-title">Аutorisation</h1>`;
        this.inputEmail = new Input("form-control","email","form-item","email","input-email","email");
        this.inputPassword = new Input("form-control","password","form-item","password","input-password","password");
        this.formBody = `<button class="login-button">Login</button>`;
    }


    render(){
        super.render();
        const form = this.form.render();
        const inputEmail = this.inputEmail.render();
        const inputPassword = this.inputPassword.render();
        const body = `${inputEmail}${inputPassword}${this.formBody}`
         form.innerHTML= body;
        this.elem.append(form);
        this.elem.insertAdjacentHTML("afterbegin",this.formHeader)
        return this.elem;
    }

    async loginPost(e){
        e.preventDefault();
        const loginButton = document.getElementById("nav-log-in");
        const createBtn = document.getElementById("create-card");
        const body = this.form.serializeJSON(e.target);
        const {data} = await axios.post("http://cards.danit.com.ua/login",body);
        if(data.status === "Success"){
            const text = document.getElementById("not-registered");
            if(text){
                text.remove();
            }
            if(data.token){
                localStorage.setItem("token",data.token)
                loginButton.remove();
            document.querySelector(".card-list").innerHTML = "";
            getCards();
            const loginForm = document.getElementById("login-form");
            createBtn.classList.add("active");
            loginForm.classList.toggle("active")
            }
            
            
        }else{
            const text = document.getElementById("not-registered");
            if(!text){
                 e.target.insertAdjacentHTML("beforeend",`<p id='not-registered'>Вы не зарегистрированы</p>`)
            }
        }
    }

}